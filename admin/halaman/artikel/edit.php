<fieldset>
	<legend>Edit Data</legend>

<?php
$kdartikel = @$_GET['kdartikel'];
$sql 	   = $mysqli->query("SELECT * FROM tb_artikel WHERE id_artikel='$kdartikel'") or die ($mysqli->error);
$data	   = $sql->fetch_array();

?>
	

<div style="margin-top: 50px; padding-top: 50px; padding-bottom: 50px;" class="container z-depth-3">
  <h3 class="center">Edit Data</h3>
  <form action="" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="input-field col s6 offset-s3">
          <input id="id_artikel" type="text" class="validate" name="id_artikel" value="<?php echo $data['id_artikel']; ?>">
          <label for="id_artikel">ID</label>
        </div>
     </div>
    <div class="row">
        <div class="input-field col s6 offset-s3">
          <input id="judul_artikel" type="text" class="validate" name="judul_artikel" value="<?php echo $data['judul_artikel']; ?>">
          <label for="judul_artikel">Judul</label>
        </div>
     </div>
     <div class="row">
        <div class="input-field col s6 offset-s3">
          <input id="tanggal_artikel" type="date" name="tanggal_artikel" value="<?php echo $data['tanggal_artikel']; ?>">
          <label for="tanggal_artikel"></label>
        </div>
     </div>
    <div class="row">
          <div class="input-field col s6 offset-s3">
            <textarea id="isi_artikel" class="materialize-textarea" name="isi_artikel"></textarea>
            <label for="isi_artikel">Isi</label>
          </div>
    </div>
    <div class="row">
      <div class="col s6 offset-s3">
        <div class="file-field input-field">
          <div class="btn">
            <span>File</span>
            <input type="file" name="gambar_artikel">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
          </div>
        </div> 
      </div>
    </div>
    <div class="row">
      <div class="col s6 offset-s3">
        <input type="submit" name="edit" value="Edit"/> 
        <input type="reset" name="reset" value="Reset"/>
      </div>

    </div>
  </form>
</div>



<?php
	$id_artikel  = @$_POST['id_artikel'];
	$judul_artikel  = @$_POST['judul_artikel'];
	$tanggal_artikel = @$_POST['tanggal_artikel'];
	$isi_artikel  = @$_POST['isi_artikel'];
	$sumber 	  = @$_FILES['gambar_artikel']['tmp_name'];
	$target		  = '../asset/admin/images/artikel/';
	$nama_gambar  = @$_FILES['gambar_artikel']['name'];

	$edit 		  = @$_POST['edit'];

	if($edit){
		if($judul_artikel == "" || $tanggal_artikel == "" || $isi_artikel == ""){
			?> 
			<script type="text/javascript">
			alert("Inputan Tidak Boleh Kosong");
			</script> 
			<?php
		} else {
		if($nama_gambar == ""){
			$mysqli->query("UPDATE tb_artikel SET judul_artikel='$judul_artikel', isi_artikel='$isi_artikel', tanggal_artikel='$tanggal_artikel' WHERE id_artikel='$kdartikel'") or die ($mysqli->error);
			?> 
			<script type="text/javascript">
			alert("Data Berhasil di Edit"); 
			window.location.href="?page=artikel";
			</script> 
			<?php
		} else {
			$pindah = move_uploaded_file($sumber, $target.$nama_gambar);
			if ($pindah){
				$mysqli->query("UPDATE tb_artikel SET judul_artikel='$judul_artikel', isi_artikel='$isi_artikel', tanggal_artikel='$tanggal_artikel', gambar_artikel = '$nama_gambar' WHERE id_artikel='$kdartikel'") or die ($mysqli->error);
			?> 
				<script type="text/javascript">
				alert("Data Berhasil di Edit"); 
				window.location.href="?page=artikel";
				</script> 
				<?php
				} else {
					?> <script type="text/javascript">alert("Gambar gagal diupload");</script> <?php
				}
			}
		}
	}
	?>
</fieldset>