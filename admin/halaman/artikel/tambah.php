<?php
  $carikode = $mysqli->query("SELECT max(id_artikel) FROM tb_artikel") or die ($mysqli->error);
  $datakode = $carikode->fetch_array();
    if($datakode) {
      $nilaikode = substr($datakode[0], 1);
      $kode      = (int) $nilaikode;
      $kode      = $kode + 1;
      $hasilkode = "A".str_pad($kode, 3, "0", STR_PAD_LEFT);
    } else {
      $hasilkode = "A001";
    }
?>

<div style="margin-top: 50px; padding-top: 50px; padding-bottom: 50px;" class="container z-depth-3">
  <h3 class="center">Tambah Data</h3>
  <form action="" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="input-field col s6 offset-s3">
          <input id="id_artikel" type="text" class="validate" name="id_artikel">
          <label for="id_artikel">ID</label>
        </div>
     </div>
    <div class="row">
        <div class="input-field col s6 offset-s3">
          <input id="judul_artikel" type="text" class="validate" name="judul_artikel">
          <label for="judul_artikel">Judul</label>
        </div>
     </div>
     <div class="row">
        <div class="input-field col s6 offset-s3">
          <input id="tanggal_artikel" type="date" name="tanggal_artikel">
          <label for="tanggal_artikel"></label>
        </div>
     </div>
    <div class="row">
          <div class="input-field col s6 offset-s3">
            <textarea id="isi_artikel" class="materialize-textarea" name="isi_artikel"></textarea>
            <label for="isi_artikel">Isi</label>
          </div>
    </div>
    <div class="row">
      <div class="col s6 offset-s3">
        <div class="file-field input-field">
          <div class="btn">
            <span>File</span>
            <input type="file" name="gambar_artikel">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
          </div>
        </div> 
      </div>
    </div>
    <div class="row">
      <div class="col s6 offset-s3">
        <input type="submit" name="tambah" value="Tambah"/> 
        <input type="reset" name="reset" value="Reset"/>
      </div>

    </div>
  </form>
</div>

<?php
  $id_artikel      = @$_POST['id_artikel'];
  $judul_artikel   = @$_POST['judul_artikel'];
  $tanggal_artikel = @$_POST['tanggal_artikel'];
  $isi_artikel     = @$_POST['isi_artikel'];
  $sumber     = @$_FILES['gambar_artikel']['tmp_name'];
  $target     = '../asset/admin/images/artikel/';
  $nama_gambar= @$_FILES['gambar_artikel']['name'];

  $tambah     = @$_POST['tambah'];

  if($tambah){
    if($id_artikel == "" || $judul_artikel == "" || $tanggal_artikel == "" || $isi_artikel == "" || $nama_gambar == ""){
      ?> <script type="text/javascript">alert("Inputan tidak boleh ada yang kosong!");</script> <?php
    } else {
    $pindah = move_uploaded_file($sumber, $target.$nama_gambar);
    if ($pindah){
      $sql = mysqli_query($mysqli, 
        "INSERT INTO tb_artikel VALUES 
        ('$id_artikel','$judul_artikel','$tanggal_artikel','$isi_artikel','$nama_gambar')") 
        or die ($mysqli->error);
      ?> <script type="text/javascript">alert("Inputan berhasil!"); window.location.href="?page=artikel";</script> <?php
      } else {
        ?> <script type="text/javascript">alert("Gambar gagal diupload");</script> <?php
      }
    }
  }
  ?>