<?php
@session_start();

if(@$_SESSION ['admin']){
?>


  <div class="container">
    <h3>Daftar Artikel</h3>
      <a href="index.php?page=artikel&aksi=tambah">
      <button type="button" class="btn blue">Tambah</button>
    </a>
        <br><br>  
    <table class="striped">
    
      <thead class="">
        <th class="center">No</th>
        <th class="center">Judul</th>
        <th class="center">Isi</th>
        <th class="center">Tanggal Pembuatan</th>
        <th style="margin-left: 50px;" class="center">Gambar</th>
        <th class="center">Aksi</th>
      </thead>

      <?php $nomor = 1; ?>  
      <?php
        $sql = $mysqli->query("SELECT * FROM tb_artikel") or die ($mysqli->error);
        while($data = $sql->fetch_array()){
      ?>
        <tr>
          <td class="center"><?php echo $nomor; ?></td>
          <td class="center"><?php echo $data['judul_artikel']; ?></td>
          <td class="center"><?php echo $data['isi_artikel'];?></td>
          <td class="center"><?php echo $data['tanggal_artikel']; ?></td>
          <td style="margin-left: 50px;" class="center"><img src="../asset/admin/images/artikel/<?php echo $data['gambar_artikel']; ?>"</td>
          <td style="width: 250px;" class="center">
            <span><a href="?page=artikel&aksi=edit&kdartikel=<?php echo $data['id_artikel']; ?>" class= "btn black">Edit</a>
            <span><a onclick="return confirm('Yakin ingin menghapus data ?')" href="?page=artikel&aksi=hapus&kdartikel=<?php echo $data['id_artikel']; ?>" class= "btn red">Hapus</a>
            
          </td>
        </tr>
        <?php $nomor++; ?>
        <?php
        }
        ?>
    </table>
  </div>
<?php
} else {
  header("location: ../../akses/login.php");
}
?>