<?php
@session_start();

if(@$_SESSION ['admin']){
?>


  <div class="container">
    <h3>Daftar Member</h3>  
    <table class="striped">
    
      <thead class="">
        <th class="center">No</th>
        <th class="center">Nama Lengkap</th>
        <th class="center">Username</th>
        <th class="center">Email</th>
        <th class="center">Password</th>
        <th class="center">Level</th>
        <th class="center">Aksi</th>
      </thead>

      <?php $nomor = 1; ?>  
      <?php
        $sql = $mysqli->query("SELECT * FROM tb_login") or die ($mysqli->error);
        while($data = $sql->fetch_array()){
      ?>
        <tr>
          <td class="center"><?php echo $nomor; ?></td>
          <td class="center"><?php echo $data['nama_lengkap']; ?></td>
          <td class="center"><?php echo $data['username'];?></td>
          <td class="center"><?php echo $data['email']; ?></td>
          <td class="center"><?php echo $data['password'];?></td>
          <td class="center"><?php echo $data['level']; ?></td>
          <td style="width: 600px;" class="center">
            <span><a onclick="return confirm('Yakin ingin menghapus data ?')" href="?page=member&aksi=hapus&iduser=<?php echo $data['id_user']; ?>" class= "btn btn-danger">Hapus</a></span>
            
          </td>
        </tr>
        <?php $nomor++; ?>
        <?php
        }
        ?>
    </table>
  </div>
<?php
} else {
  header("location: ../../akses/login.php");
}
?>