<?php
@session_start();
include('../server/koneksi.php');

if(@$_SESSION ['admin']){
?>


<!DOCTYPE html>
<html lang="en">
  <!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 4.0
	Author: PIXINVENT
	Author URL: https://themeforest.net/user/pixinvent/portfolio
  ================================================================================ -->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Halaman Admin</title>
    <!-- Favicons-->
    <link rel="icon" href="images/favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="../asset/admin/images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="../asset/admin/images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->
    <!-- CORE CSS-->
    <link href="../asset/admin/css/materialize.css" type="text/css" rel="stylesheet">
    <link href="../asset/admin/css/style.css" type="text/css" rel="stylesheet">
    <!-- Custome CSS-->
    <link href="../asset/admin/css/custom/custom.css" type="text/css" rel="stylesheet">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="../asset/admin/vendors/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet">
    <link href="../asset/admin/vendors/flag-icon/css/flag-icon.min.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../asset/admin/css/custom/custom.css">
  </head>
  <body>


    <!-- Start Page Loading -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START HEADER -->
    <header id="header" class="page-topbar">
      <!-- start header nav-->
      <div class="navbar-fixed">
        <nav class="navbar-color gradient-45deg-light-blue-cyan">
          <div class="nav-wrapper">
            <ul class="left">
              <li>
                <h1 class="logo-wrapper">
                  <a href="index.html" class="brand-logo darken-1">
                    <img src="../asset/admin/images/logo/materialize-logo.png" alt="materialize logo">
                    <span class="logo-text hide-on-med-and-down">Administrator</span>
                  </a>
                </h1>
              </li>
            </ul>
            <div class="header-search-wrapper hide-on-med-and-down">
              <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Search" />
            </div>
        </nav>
      </div>
      <!-- end header nav-->
    </header>
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <aside id="left-sidebar-nav">
          <ul id="slide-out" class="side-nav fixed leftside-navigation">
            <li class="user-details cyan darken-2">
              <div class="row">
                <div class="col col s4 m4 l4">
                  <img src="../asset/admin/images/avatar/avatar-7.png" alt="" class="circle responsive-img valign profile-image cyan">
                </div>
                <div class="col col s8 m8 l8">
                  <span class="white-text">Selamat Datang!<i class="mdi-navigation-arrow-drop-down right"></i></span>
                </div>
              </div>
            </li>
            <li class="no-padding">
              <ul class="collapsible" data-collapsible="accordion">
                <li class="bold">
                  <a href="index.php" class="waves-effect waves-cyan">
                      <span class="nav-text">Dashboard</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="index.php?page=member" class="waves-effect waves-cyan">
                      <span class="nav-text">Member</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="index.php?page=artikel" class="waves-effect waves-cyan">
                      <span class="nav-text">Artikel</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="../akses/logout.php" class="btn">Logout</a>
                </li>
          </ul>
        </aside>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- Page -->
       <!--  <?php 
          if(isset($_GET['page'])){
            $page = $_GET['page'];
            $aksi = $_GET['aksi'];
         
            switch ($page) {
              case 'dashboard':
                include "dashboard.php";
                break; 
              case 'admin':
                include "halaman/admin/list.php";
                break;
              case 'member':
                include "halaman/member/list.php";
                break;
              case 'artikel':
                include "halaman/artikel/artikel.php";
                break;    
              default:
                echo "<center><h3>Maaf. Halaman tidak di temukan !</h3></center>";
                break;
            }
          }else{
            include "dashboard.php";
          }
         
        ?>   -->
        <!-- Akhir Page -->

      <?php
      $page   = @$_GET['page'];
      $aksi = @$_GET['aksi'];

      if($page == "member") {
          if($aksi == "") {
          include "halaman/member/list.php";
      } else if($aksi == "hapus") {
          include "halaman/member/hapus.php";
          }
      } else if($page == "artikel") {
          if($aksi == "") {
          include "halaman/artikel/list.php";
          } else if($aksi == "tambah") {
          include "halaman/artikel/tambah.php";
          } else if($aksi == "edit") {
          include "halaman/artikel/edit.php";
          } else if($aksi == "hapus") {
          include "halaman/artikel/hapus.php";
          }
      } else if($page == "") {
          include"halaman/member/dashboard.php";
      }

    ?>
        <!-- //////////////////////////////////////////////////////////////////////////// -->
      </div>
      <!-- END WRAPPER -->
    </div>
    <!-- END MAIN -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START FOOTER -->
    <footer class="page-footer gradient-45deg-light-blue-cyan">
        <div class="footer-copyright">
          <div class="container">
            <span>Copyright ©
              <script type="text/javascript">
                document.write(new Date().getFullYear());
              </script> <a class="grey-text text-lighten-2" href="http://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank">PIXINVENT</a> All rights reserved.</span>
            <span class="right hide-on-small-only"> Design and Developed by <a class="grey-text text-lighten-2" href="https://pixinvent.com/">PIXINVENT</a></span>
          </div>
        </div>
    </footer>
    <!-- END FOOTER -->
    <!-- ================================================
    Scripts
    ================================================ -->
    <!-- jQuery Library -->
    <script type="text/javascript" src="../asset/admin/vendors/jquery-3.2.1.min.js"></script>
    <!--materialize js-->
    <script type="text/javascript" src="../asset/admin/js/materialize.min.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../asset/admin/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../asset/admin/js/plugins.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../asset/admin/js/custom-script.js"></script>
  </body>
</html>
<?php
} else {
  header("location: ../akses/login.php");
}
?>