<!DOCTYPE html>
  <html>
    <head>
      <title>Login</title>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="../asset/user/css/materialize.min.css"  media="screen,projection"/>
      <link href="https://fonts.googleapis.com/css?family=Tauri" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="../asset/user/css/gayalogin.css">

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
  
<body>
  <?php 
  if(isset($_GET['pesan'])){
    if($_GET['pesan']=="gagal"){
      ?> <script type="text/javascript">alert("Username dan Password tidak sesuai");</script> <?php
    } 
  }
  ?>
  <!-- login -->
      <div class="container z-depth-3">
        <form action="../pagekontrol/cek_login.php" method="post">
        <div class="row">
          <div class="col s12">
            <div class="kotak light-blue darken-1">              
              <h1 class="center">LOGIN</h1>
            </div>
          </div>
        </div>
        <div class="row">
          <form class="col s12">
            <div class="row">
              <div class="input-field col s10 offset-s1 center">
                <i class="material-icons prefix">account_circle</i>
                <input id="username" type="text" class="validate" name="username">
                <label for="username">Username</label>
              </div>
            </div>
          </form>
          <div class="input-field col s10 offset-s1 center">
            <i class="material-icons prefix">lock</i>
            <input id="password" type="password" class="validate" name="password">
            <label for="password">Password</label>
          </div>
          <div class="col s2 offset-s1">
            <input type="submit" name="login" value="Login" class="btn btn-danger"></input>
          </div>
          <div class="col s3 offset-s8">
            <a href="daftar.php">belum punya akun?</a>
          </div>
        </div>
      </div>
      <!-- akhir login -->


      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="../asset/user/js/materialize.min.js"></script>
      <script type="text/javascript" src="../asset/user/js/jslogin.js"></script>
    </body>
  </html>