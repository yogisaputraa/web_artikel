<!DOCTYPE html>
<html>
  <head>
    <title>Daftar</title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="../asset/user/css/materialize.min.css"  media="screen,projection"/>
    <link href="https://fonts.googleapis.com/css?family=Tauri" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../asset/user/css/gayadaftar.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>

  <body>
    <!-- login -->
    <div class="container z-depth-3">
      <form action="../pagekontrol/proses_daftar.php" method="post">
      <div class="row">
        <div class="col s12">
            <h1 class="center">Silahkan Daftar disini</h1>
        </div>
      </div>
      <div class="row">
        <form class="col s12">
          <div class="row">
            <div class="input-field col s10 offset-s1 center">
              <i class="material-icons prefix">account_circle</i>
              <input id="icon_prefix" type="text" class="validate" name="nama_lengkap">
              <label for="icon_prefix">Nama Lengkap</label>
            </div>
            <div class="input-field col s10 offset-s1 center">
              <i class="material-icons prefix">account_circle</i>
              <input id="email" type="text" class="validate" name="username">
              <label for="email">Username</label>
            </div>
            <div class="input-field col s10 offset-s1 center">
              <i class="material-icons prefix">email</i>
              <input id="email" type="email" class="validate" name="email">
              <label for="email">E-mail</label>
            </div>
            <div class="input-field col s10 offset-s1 center">
              <i class="material-icons prefix">lock</i>
              <input id="password" type="password" class="validate" name="password">
              <label for="password">Password</label>
            </div>
            <div class="input-field col s10 offset-s1 center">
              <i class="material-icons prefix">lock</i>
              <input id="password" type="password" class="validate" name="confirm">
              <label for="password">Confirm Password</label>
            </div>
          </div>
        </form>
        <div class="col s2 offset-s1">
          <input type="submit" name="daftar" value="Daftar" class="btn btn-danger"></input>
        </div>
        <div class="col s3 offset-s8">
          <a href="login.php">sudah punya akun?</a>
        </div>
      </div>
    </div>
    <!-- akhir login -->


    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../asset/user/js/materialize.min.js"></script>
    <script type="text/javascript" src="../asset/user/js/jslogin.js"></script>
  </body>
</html>
