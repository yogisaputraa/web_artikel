<?php
@session_start();
include('server/koneksi.php');

if(@$_SESSION ['user']) {
?>

<!DOCTYPE html>
  <html>
    <title>Beranda</title>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="asset/user/css/materialize.min.css"  media="screen,projection"/>
      <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="asset/user/css/style.css">

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>


      <!-- navbar -->
      <div class="navbar-fixed">
        <nav class="blue darken-2">
          <div class="container">
            <div class="nav-wrapper">
              <a href="#" class="brand-logo">Logo</a>
              <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
              <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="index.php">Beranda</a></li>
                <li><a href="http://localhost/webartikel/memberlist.php">Memberlist</a></li>
                <li><a href="http://localhost/webartikel/gallery.html">Gallery</a></li>
                <li><a href="http://localhost/webartikel/berita.php">Berita</a></li>
                <li><a href="akses/logout.php">Logout</a></li>
              </ul>
              <!-- mobile sidenav -->
              <ul class="side-nav" id="mobile-demo">
                <li><a href="index.php">Beranda</a></li>
                <li><a href="http://localhost/webartikel/memberlist.php">Memberlist</a></li>
                <li><a href="http://localhost/webartikel/gallery.html">Gallery</a></li>
                <li><a href="http://localhost/webartikel/berita.php">Berita</a></li>
                <li><a href="akses/logout.php">Logout</a></li>
              </ul>
              <!-- akhir mobile sidenav -->
            </div>
          </div>
        </nav>
      </div>
      <!-- akhir navbar -->


      <!-- awal parallax -->
      <div class="parallax-container">
        <div class="parallax"><img src="asset/user/img/g5.jpg"></div>
      </div>
      <!-- akhir parallax -->


      <!-- gallery -->
      <section class="gallery">
        <div class="container">
          <div class="row">
            <div class="col s12">
              <h1 class="center">Gallery</h1>
            </div>
            <div class="col s4">
              <img class="materialboxed z-depth-3" width="400" src="asset/user/img/bk1.jpg">
            </div>
            <div class="col s4">
              <img class="materialboxed z-depth-3" width="400" src="asset/user/img/bk2.jpg">
            </div>
            <div class="col s4">
              <img class="materialboxed z-depth-3" width="400" src="asset/user/img/bk3.jpg">
            </div>
            <div class="col s12 center">
               <a class="waves-effect waves-light btn blue darken-2 yes" href="http://localhost/webartikel/gallery.html">Lihat Semua Foto</a>
             </div>
          </div>
        </div>
      </section>
      <!-- akhir gallery -->


      <!-- berita -->
      <section class="berita">
        <div class="container">
           <div class="row">
             <div class="col s12">
               <h1 class="center">Berita</h1>
             </div>
             <div class="col s3">
               <div class="card">
                  <?php
                    $page = @$_GET['page'];
                      if ($page == "berita") {
                      include "berita.php";
                    } else {
                      $sql = $mysqli->query("SELECT * FROM tb_artikel") or die ($mysqli->error);
                      while($data = $sql->fetch_array()){
                  ?>
                  <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator" src="asset/admin/images/artikel/<?php echo $data['gambar_artikel']; ?>">
                  </div>
                  <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4"><?php echo $data['judul_artikel']; ?><i class="material-icons right">more_vert</i></span>
                    <p><a href="detail.php?page=berita&detail=<?php echo $data['id_artikel']; ?>">Lihat Selengkapnya</a></p>
                  </div>
                  <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i>BERITA</span>
                    <p><?php echo substr($data['isi_artikel'],0,189); ?>...</p>
                  </div>
                  <?php
                  }
                  }
                  ?>
                </div>
             </div>
             <div class="col s3">
               <div class="card">
                  <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator" src="asset/user/img/g2.jpg">
                  </div>
                  <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">Berita 2<i class="material-icons right">more_vert</i></span>
                    <p><a href="file:///C:/Users/Rudi/Desktop/materialize/detailart2.html">Lihat Selengkapnya</a></p>
                  </div>
                  <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i>BERITA</span>
                    <p>Here is some more information about this product that is only revealed once clicked on.</p>
                  </div>
                </div>
             </div>
             <div class="col s3">
               <div class="card">
                  <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator" src="asset/user/img/g3.jpg">
                  </div>
                  <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">Berita 3<i class="material-icons right">more_vert</i></span>
                    <p><a href="file:///C:/Users/Rudi/Desktop/materialize/detailart3.html">Lihat Selengkapnya</a></p>
                  </div>
                  <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i>BERITA</span>
                    <p>Here is some more information about this product that is only revealed once clicked on.</p>
                  </div>
                </div>
             </div>
             <div class="col s3">
               <div class="card">
                  <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator" src="asset/user/img/g2.jpg">
                  </div>
                  <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">Berita 2<i class="material-icons right">more_vert</i></span>
                    <p><a href="file:///C:/Users/Rudi/Desktop/materialize/detailart2.html">Lihat Selengkapnya</a></p>
                  </div>
                  <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i>BERITA</span>
                    <p>Here is some more information about this product that is only revealed once clicked on.</p>
                  </div>
                </div>
             </div>
             <div class="col s12 center">
               <a class="waves-effect waves-light btn blue darken-2 ok" href="berita.php">Lihat Semua Berita</a>
             </div>
           </div>
        </div>
      </section>
      <!-- akhir berita -->


      <!-- footer -->
      <footer class="page-footer grey darken-3">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Footer Content</h5>
                <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2019 Copyright by Anonymus.
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
          </div>
        </footer>
      <!-- akhir footer -->



      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="asset/user/js/materialize.min.js"></script>
      <script type="text/javascript" src="asset/user/js/javascript.js"></script>

    </body>
  </html>

<?php
} else {
  header("location: akses/login.php");
}
?>