<?php
@session_start();
include('server/koneksi.php');

if(@$_SESSION ['user']) {
?>


<!DOCTYPE html>
  <html>
    <head>
      <title>Member</title>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="asset/user/css/materialize.min.css"  media="screen,projection"/>
      <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="asset/user/css/memberlist.css">

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>

      <!-- navbar -->
      <div class="navbar-fixed">
        <nav class="blue darken-2">
          <div class="container">
            <div class="nav-wrapper">
              <a href="#" class="brand-logo">Logo</a>
              <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
              <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="index.php">Beranda</a></li>
                <li><a href="http://localhost/webartikel/memberlist.php">Memberlist</a></li>
                <li><a href="http://localhost/webartikel/gallery.html">Gallery</a></li>
                <li><a href="http://localhost/webartikel/berita.php">Berita</a></li>
                <li><a href="akses/logout.php">Logout</a></li>
              </ul>
              <!-- mobile sidenav -->
              <ul class="side-nav" id="mobile-demo">
                <li><a href="index.php">Beranda</a></li>
                <li><a href="http://localhost/webartikel/memberlist.php">Memberlist</a></li>
                <li><a href="http://localhost/webartikel/gallery.html">Gallery</a></li>
                <li><a href="http://localhost/webartikel/berita.php">Berita</a></li>
                <li><a href="akses/logout.php">Logout</a></li>
              </ul>
              <!-- akhir mobile sidenav -->
            </div>
          </div>
        </nav>
      </div>
      <!-- akhir navbar -->


      <!-- memberlist -->
      <section class="memberlist">
        <div class="container">
        <h1 class="center">Data Member</h1>
         <?php
            $sql = $mysqli->query("SELECT * FROM tb_login") or die ($mysqli->error);
            while($data = $sql->fetch_array()){
          ?>
          <ul class="collection">
            <li class="collection-item avatar">
              <img src="asset/user/img/avatar.png" alt="" class="circle">
              <span class="title"><?php echo $data['username']; ?></span>
              <p><?php echo $data['email']; ?>
              </p>
            </li>
          </ul>
          <?php
          }
          ?>
        </div>
      </section>
      <!-- memberlist -->


      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="asset/user/js/materialize.min.js"></script>
      <script type="text/javascript" src="asset/user/js/javascript.js"></script>

    </body>
  </html>
<?php
} else {
  header("location: akses/login.php");
}
?>