<?php
@session_start();
include('server/koneksi.php');

if(@$_SESSION ['user']) {
?>


<!DOCTYPE html>
  <html>
    <head>
      <title>Berita</title>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="asset/user/css/materialize.min.css"  media="screen,projection"/>
      <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="asset/user/css/detail.css">

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>


      <!-- navbar -->
      <div class="navbar-fixed">
        <nav class="blue darken-2">
          <div class="container">
            <div class="nav-wrapper">
              <a href="#" class="brand-logo">Logo</a>
              <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
              <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="index.php">Beranda</a></li>
                <li><a href="http://localhost/webartikel/memberlist.php">Memberlist</a></li>
                <li><a href="http://localhost/webartikel/gallery.html">Gallery</a></li>
                <li><a href="http://localhost/webartikel/berita.php">Berita</a></li>
                <li><a href="akses/logout.php">Logout</a></li>
              </ul>
              <!-- mobile sidenav -->
              <ul class="side-nav" id="mobile-demo">
                <li><a href="index.php">Beranda</a></li>
                <li><a href="http://localhost/webartikel/memberlist.php">Memberlist</a></li>
                <li><a href="http://localhost/webartikel/gallery.html">Gallery</a></li>
                <li><a href="http://localhost/webartikel/berita.php">Berita</a></li>
                <li><a href="akses/logout.php">Logout</a></li>
              </ul>
              <!-- akhir mobile sidenav -->
            </div>
          </div>
        </nav>
      </div>
      <!-- akhir navbar -->


      <!-- list detail -->
      <section class="listdetail">
        <div class="container">
          <div class="col s12">
             <?php
                   $sql = $mysqli->query("SELECT * FROM tb_artikel") or die ($mysqli->error);
                    while($data = $sql->fetch_array()){
              ?>
            <h1 class="center"><?php echo $data['judul_artikel']; ?></h1>
              <div class="card horizontal">
                <div class="card-image">
                  <img src="asset/admin/images/artikel/<?php echo $data['gambar_artikel']; ?>">
                </div>
                  <div class="card-stacked">
                    <div class="card-content">
                      <p><?php echo substr($data['isi_artikel'],0,170); ?></p>
                    </div>
                    <div class="card-action">
                      <a href="detail.php?page=berita&detail=<?php echo $data['id_artikel']; ?>" class="btn blue darken-1">Lihat Selengkapnya</a>
                    </div>
                  </div>
              </div>
              <?php
              }
              ?>
          </div>
        </div>
      </section>
      <!-- akhir list detail -->


      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="asset/user/js/materialize.min.js"></script>
      <script type="text/javascript" src="asset/user/js/javascript.js"></script>

    </body>
  </html>
<?php
} else {
  header("location: akses/login.php");
}
?>