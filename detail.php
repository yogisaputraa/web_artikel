<?php
@session_start();
include('server/koneksi.php');

if(@$_SESSION ['user']) {
?>

<!DOCTYPE html>
  <html>
    <head>
      <title>Detail</title>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="asset/user/css/materialize.min.css"  media="screen,projection"/>
      <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="asset/user/css/gayadetail.css">

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>


      <!-- navbar -->
      <div class="navbar-fixed">
        <nav class="blue darken-2">
          <div class="container">
            <div class="nav-wrapper">
              <a href="#" class="brand-logo">Logo</a>
              <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
              <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="index.php">Beranda</a></li>
                <li><a href="http://localhost/webartikel/memberlist.php">Memberlist</a></li>
                <li><a href="http://localhost/webartikel/gallery.html">Gallery</a></li>
                <li><a href="http://localhost/webartikel/berita.php">Berita</a></li>
                <li><a href="akses/logout.php">Logout</a></li>
              </ul>
              <!-- mobile sidenav -->
              <ul class="side-nav" id="mobile-demo">
                <li><a href="index.php">Beranda</a></li>
                <li><a href="http://localhost/webartikel/memberlist.php">Memberlist</a></li>
                <li><a href="http://localhost/webartikel/gallery.html">Gallery</a></li>
                <li><a href="http://localhost/webartikel/berita.php">Berita</a></li>
                <li><a href="akses/logout.php">Logout</a></li>
              </ul>
              <!-- akhir mobile sidenav -->
            </div>
          </div>
        </nav>
      </div>
      <!-- akhir navbar -->


      <!-- detail berita -->
      <section class="detailberita">
        <h1 class="center">Berita</h1>
        <div class="container z-depth-3">
          <?php
          $kdartikel = @$_GET['detail'];
          $sql = $mysqli->query("SELECT * FROM tb_artikel WHERE id_artikel='$kdartikel'") or die ($mysqli->error);
            while($data = $sql->fetch_array()){
          ?>
            <h4><?php echo $data['judul_artikel']; ?></h4>
            <p class="tanggal"><?php echo $data['tanggal_artikel']; ?></p>
          <div class="row">
            <div class="col s12 center">
              <img src="asset/admin/images/artikel/<?php echo $data['gambar_artikel']; ?>">
              <p><?php echo $data['isi_artikel']; ?></p>
            </div>
          </div>
          <?php
          }
          ?>
        </div>            
      </section>
      <!-- akhir detail berita -->


      <!-- footer -->
      <footer class="page-footer grey darken-3">
        <div class="container">
          <div class="row">
            <div class="col l6 s12">
              <h5 class="white-text">Footer Content</h5>
              <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
            </div>
            <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
               <ul>
                <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
                <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
                <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
               </ul>
            </div>
          </div>
        </div>
          <div class="footer-copyright">
            <div class="container">
            © 2019 Copyright by Anonymus.
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
          </div>
      </footer>
      <!-- akhir footer -->


      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="asset/user/js/materialize.min.js"></script>
      <script type="text/javascript" src="asset/user/js/javascript.js"></script>

    </body>
  </html>
<?php
} else {
  header("location: akses/login.php");
}
?>